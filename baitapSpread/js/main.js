let content = document.querySelector(".heading").textContent;
console.log("content: ", content);

let chars = [...content];
console.log("chars: ", chars);

// way 1
// Set text for span element
const spanTags = chars.map((character) => `<span>${character}</span>`);
document.querySelector(".heading").innerHTML = spanTags.join("");

// // way 2
// // Refresh content when characters are get out of array
// document.querySelector(".heading").textContent = "";

// // Get through all characters in array
// chars.forEach((character) => {
//   // check characters in "chars" array by console log
//   console.log("character: ", character);

//   // add span tags in h2 tags
//   let addSpan = document.createElement("span");

//   // Set text content for each span element
//   addSpan.innerHTML = `${character}`;

//   // Create span tags in "heading" class
//   document.querySelector(".heading").appendChild(addSpan);
// });
