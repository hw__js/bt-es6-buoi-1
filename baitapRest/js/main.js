document.getElementById("btnKhoi1").addEventListener("click", () => {
  let mathScore = document.getElementById("inpToan").value.trim();
  let physicsScore = document.getElementById("inpLy").value.trim();
  let chemistryScore = document.getElementById("inpHoa").value.trim();

  let averageScore1 = averageCal(
    mathScore * 1,
    physicsScore * 1,
    chemistryScore * 1
  );

  document.getElementById("tbKhoi1").innerHTML = averageScore1.toFixed(2);
});

document.getElementById("btnKhoi2").addEventListener("click", () => {
  let literatureScore = document.getElementById("inpVan").value.trim();
  let historyScore = document.getElementById("inpSu").value.trim();
  let geographyScore = document.getElementById("inpDia").value.trim();
  let englishScore = document.getElementById("inpEnglish").value.trim();

  let averageScore2 = averageCal(
    literatureScore * 1,
    historyScore * 1,
    geographyScore * 1,
    englishScore * 1
  );

  document.getElementById("tbKhoi2").innerHTML = averageScore2.toFixed(2);
});

const averageCal = (...input) => {
  if (input.length === 0) return 0;
  let sum = 0;
  for (let i of input) {
    sum += i;
  }
  return sum / input.length;
};
