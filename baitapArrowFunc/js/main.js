const colorList = [
  "pallet",
  "viridian",
  "pewter",
  "cerulean",
  "vermillion",
  "lavender",
  "celadon",
  "saffron",
  "fuschia",
  "cinnabar",
];

// List color button on HTML file
const showColor = () => {
  // traverse the array's colours one by one
  const addBtnColor = colorList.map(
    (color) =>
      `<button class="color-button ${color}" data-color="${color}"></button>`
  );
  document.getElementById("colorContainer").innerHTML = addBtnColor.join("");
};
showColor();

const activeButton = () => {
  // Get the container element
  let btnContainer = document.getElementById("colorContainer");

  // Get all buttons with class="color-button" inside the container
  let btns = btnContainer.getElementsByClassName("color-button");

  // Loop through the buttons and add the active class to the current/clicked button
  for (var i = 0; i < btns.length; i++) {
    btns[i].addEventListener("click", function () {
      var current = document.getElementsByClassName("active");

      // If there's no active class
      if (current.length > 0) {
        current[0].className = current[0].className.replace(" active", "");
      }

      // Add the active class to the current/clicked button
      this.className += " active";
    });
  }
};

// Get all buttons
const btnColor = document.querySelectorAll(".color-button");

// Loop through the buttons
btnColor.forEach((btn) => {
  console.log("btn: ", btn);

  btn.addEventListener("click", function onClick() {
    // Let color value from button
    let color = btn.getAttribute("data-color");
    console.log("color: ", color);

    document
      .getElementById("house")
      .classList.remove(
        "pallet",
        "viridian",
        "pewter",
        "cerulean",
        "vermillion",
        "lavender",
        "celadon",
        "saffron",
        "fuschia",
        "cinnabar"
      );
    document.getElementById("house").classList.add(color);
  });
});
